












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Multiply arrays vec1 and vec2 element by element. Output on vec2.
! The product is done efficiently with the use of linear algebra
! libraries (assuming that they were optimized for the current machine).
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine dmultiply_vec(ndata,vec1,vec2)

  use myconstants
  implicit none
  ! length of arrays
  integer, intent(in) :: ndata
  ! array vec1
  real(dp), dimension(ndata), intent(in) :: vec1
  ! array vec2
  ! on output, it is replaced with the product vec2(i) = vec1(i) * vec2(i)
  real(dp), dimension(ndata), intent(inout) :: vec2

! BLAS specific
  call dtbmv('U','N','N',ndata,0,vec1,1,vec2,1)

end subroutine dmultiply_vec
!===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Multiply arrays vec1 and vec2 element by element. Output on vec2.
! The product is done efficiently with the use of linear algebra
! libraries (assuming that they were optimized for the current machine).
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zmultiply_vec(ndata,vec1,vec2)

  use myconstants
  implicit none
  ! length of arrays
  integer, intent(in) :: ndata
  ! array vec1
  complex(dpc), dimension(ndata), intent(in) :: vec1
  ! array vec2
  ! on output, it is replaced with the product vec2(i) = vec1(i) * vec2(i)
  complex(dpc), dimension(ndata), intent(inout) :: vec2

! BLAS specific
  call ztbmv('U','N','N',ndata,0,vec1,1,vec2,1)

end subroutine zmultiply_vec
!===================================================================
